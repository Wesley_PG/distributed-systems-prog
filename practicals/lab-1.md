# Lab 1

The goal of this lab is to introduce *Ballerina*, the programming language and its runtime stack. As a starting point, an overview is provided to you as well.

## Problem 1

Go to *Ballerina* website, download it and complete the installation on your machine.

## Problem 2

Follow the links in the overview document and read about the design principles behind *Ballerina* as well as its runtime architecture.

## Problem 3

Ballerina comes with a set of command line interfaces presented [here](https://ballerina.io/learn/using-the-cli-tools/). Choose the appropriate commands to create a project. You will change the content of the main file to display your student and course details (student number, fullname, course code and designation).

Add the *git* protocol to your project and deploy it to [Gitlab](https://gitlab.com).
